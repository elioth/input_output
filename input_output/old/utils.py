from oemof.outputlib import views
import pandas as pd
import numpy as np
import os
from scipy.sparse import coo_matrix


# Extract all flows and compute the footprint of final consumption flows
# def extract_footprint(s, timestep, package_path, dests=["french"]):

    # # Extract all flows from the system
    # flows_list = []
    
    # for t in ['row', 'france', 'neighborhood']:
        # for service, node in s.nodes['territories'][t]['services'].items():
            # flows = views.node(s.results, t + '_' + service + '_node')['sequences'].T
            # flows = flows.iloc[:, timestep:(timestep+1)].copy()
            # flows.reset_index(inplace=True)
            # flows.columns = ['flow', 'value']
            # flows['from'] = flows['flow'].apply(lambda x: x[0][0])
            # flows['to'] = flows['flow'].apply(lambda x: x[0][1])
            # flows = flows[['from', 'to', 'value']]
            # flows_list.append(flows)
    
    # for service, node in s.nodes['biosphere']['services'].items():
            # flows = views.node(s.results, 'biosphere_' + service + '_node')['sequences'].T
            # flows = flows.iloc[:, timestep:(timestep+1)].copy()
            # flows.reset_index(inplace=True)
            # flows.columns = ['flow', 'value']
            # flows['from'] = flows['flow'].apply(lambda x: x[0][0])
            # flows['to'] = flows['flow'].apply(lambda x: x[0][1])
            # flows = flows[['from', 'to', 'value']]
            # flows_list.append(flows)
            
    # flows = pd.concat(flows_list)
    # flows = flows[flows['value'] > 0]
    
    # # Input / output table
    # io = flows.pivot_table(index='from', columns='to', values='value', fill_value=0)
    # io.to_excel('io.xlsx')
    
    
    
    # products = pd.read_excel(os.path.join(package_path, 'input_output', 'data', 'metadata.xlsx'), sheet_name='product')
    
    # # Index the flows
    # nodes_labels = np.unique(np.hstack((flows['from'], flows['to'])))
    # nodes_index = pd.DataFrame({'label': nodes_labels, 'index': np.arange(0, len(nodes_labels)) })
    
    # flows = pd.merge(flows, nodes_index, left_on = 'from', right_on = 'label')
    # flows = pd.merge(flows, nodes_index, left_on = 'to', right_on = 'label')
    

    # # Compute the technical coefficients of each flow (a_ij = input_i / total_output_j)
    # a = flows.copy()

    # p = a.groupby('from', as_index=False)['value'].sum()
    # p.columns = ['to', 'total_supply']
    
    # a = pd.merge(a, p, on='to')
    # a['a_ij'] = a['value']/a['total_supply']
    
    # # Extract co2 emissions and compute the direct carbon intensity of each flow
    # co2 = flows[flows['from'] == 'biosphere_CO2E_node']
    # co2 = pd.merge(co2, p, on = 'to')
    # co2['e_j'] = co2['value']/co2['total_supply']
    
    # # Compute the emission factor of each flow
    # # Create the technical coefficients matrix
    # N = nodes_index.shape[0]
    # a_mat = coo_matrix((a['a_ij'], (a['index_y'], a['index_x'])), shape=(N, N))
    # a_mat.todense().shape
    
    # # Create the direct emissions vector
    # co2_mat = coo_matrix((co2['e_j'], (co2['index_y'], np.zeros(co2.shape[0]).astype(int))), shape = (N, 1))
    
    # results = nodes_index.copy()
    # results['emission_factor'] = np.matmul(np.linalg.inv(np.eye(N) - a_mat.todense()), co2_mat.todense()[:, 0])

    # for dest in dests:
        # # Extract the consumption flows and compute their allocated emissions
        # hh = flows[flows['to'] == dest]
        # hh_mat = coo_matrix((hh['value'], (hh['index_x'], np.zeros(hh.shape[0]).astype(int))), shape = (N, 1))
        

        # results[dest] = results['emission_factor'].values*hh_mat.toarray()[:, 0]
        
        # results = results[results['label'].str.contains('node')].copy()
        # results.loc[results['label'].str.contains('row'), 'territory'] = 'row'
        # results.loc[results['label'].str.contains('france'), 'territory'] = 'france'
        # results.loc[results['label'].str.contains('neighborhood'), 'territory'] = 'neighborhood'
        # results['product_id'] = results['label'].str.replace('france_|row_|neighborhood_|_node', '')
        
        # results = pd.merge(results, products, on = 'product_id')
        
        # results[['product_id', 'code', 'label_fr', 'territory', dest]].to_excel('footprint_{}.xlsx'.format(dest))


def extract_footprint(s, timestep, package_path, users=["households"],overwrite=True):

    # Extract all non zero flows from the system
    flows = s.results[s.results['value'] > 0].copy()
    # print(flows.head())
    flows = flows[flows['timestep'] == timestep]
    # print(flows.head())
    flows = flows[['from', 'to', 'value']]
    # print(flows.head())

    # Filter out dummy capital formation flows to avoid double counting
    flows = flows[~flows['from'].str.contains("capital_formation_node")]
    flows = flows[~flows['to'].str.contains("capital_formation_node")]
    
    # Input / output table
    #io = flows.pivot_table(index='from', columns='to', values='value', fill_value=0)
    flows.to_csv('io.csv')
    
    products = pd.read_excel(os.path.join(package_path, 'input_output', 'data', 'metadata.xlsx'), sheet_name='product')
    
    # Index the flows
    nodes_labels = np.unique(np.hstack((flows['from'], flows['to'])))
    nodes_index = pd.DataFrame({'label': nodes_labels, 'index': np.arange(0, len(nodes_labels)) })
    
    flows = pd.merge(flows, nodes_index, left_on = 'from', right_on = 'label')
    flows = pd.merge(flows, nodes_index, left_on = 'to', right_on = 'label')
    
    # print(flows.head())
    

    # Compute the technical coefficients of each flow (a_ij = input_i / total_output_j)
    a = flows.copy()

    p = a.groupby('from', as_index=False)['value'].sum()
    p.columns = ['to', 'total_supply']
    
    a = pd.merge(a, p, on='to')
    a['a_ij'] = a['value']/a['total_supply']
    
    # Extract co2 emissions and compute the direct carbon intensity of each flow
    co2 = flows[flows['from'] == 'biosphere_CO2E_node']
    co2 = pd.merge(co2, p, on = 'to')
    co2['e_j'] = co2['value']/co2['total_supply']
    
    # Compute the emission factor of each flow
    # Create the technical coefficients matrix
    N = nodes_index.shape[0]
    a_mat = coo_matrix((a['a_ij'], (a['index_y'], a['index_x'])), shape=(N, N))
    a_mat.todense().shape
    
    # Create the direct emissions vector
    co2_mat = coo_matrix((co2['e_j'], (co2['index_y'], np.zeros(co2.shape[0]).astype(int))), shape = (N, 1))
    
    res_list = []

    

    for user in users:
        results = nodes_index.copy()
        results['emission_factor'] = np.matmul(np.linalg.inv(np.eye(N) - a_mat.todense()), co2_mat.todense()[:, 0])
        print(user)
        # Extract the consumption flows and compute their allocated emissions
        hh = flows[flows['to'] == user]
        hh_mat = coo_matrix((hh['value'], (hh['index_x'], np.zeros(hh.shape[0]).astype(int))), shape = (N, 1))
        
    
        results[user] = results['emission_factor'].values*hh_mat.toarray()[:, 0]
        
        results = results[results['label'].str.contains('node')].copy()
        results.loc[results['label'].str.contains('row'), 'territory'] = 'row'
        results.loc[results['label'].str.contains('france'), 'territory'] = 'france'
        results.loc[results['label'].str.contains('neighborhood'), 'territory'] = 'neighborhood'
        results['product_id'] = results['label'].str.replace('france_|row_|neighborhood_|_node', '')
        
        results = pd.merge(results, products, on = 'product_id')
        results = results[['product_id', "code", 'label_fr', 'territory', user]]
        results = results.set_index(['product_id', "code", 'label_fr', 'territory'])

        res_list.append(results)
    
    results = pd.concat(res_list,axis=1)
    
    unformatted_path = "footprint{}.xlsx"
    if overwrite:
        path = unformatted_path.format("")
    else:
        path = unformatted_path.format("")
        version = 0
        while os.path.isfile(path):
            path = unformatted_path.format(version)
            version += 1
    
    results.to_excel(path)
	
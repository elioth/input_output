import sqlite3
import os
import pandas as pd

class Service:

    def __init__(self, label, conversion_factor=1.0, capacity=None, flow_type=None):
        self.label = label
        self.conversion_factor = conversion_factor
        self.capacity = capacity
        self.flow_type = flow_type


class InputService(Service):
    
    def __init__(self, label, territory, conversion_factor=1.0, capacity=None, flow_type=None):
        super().__init__(label, conversion_factor, capacity, flow_type)
        self.origin_territory = territory

class OutputService(Service):
    
    def __init__(self, label, territory, conversion_factor=1.0, capacity=None, flow_type=None):
        super().__init__(label, conversion_factor, capacity, flow_type)
        self.destination_territory = territory
        
class InternalService(Service):
    
    def __init__(self, label, conversion_factor=1.0, capacity=None, flow_type=None):
        super().__init__(label, conversion_factor, capacity, flow_type) 


class Activity:

    def __init__(self, system, label, territory, fixed, input_services=[], output_services=[], internal_services=[]):
        self.system = system
        self.label = label
        self.territory = territory
        self.fixed = fixed
        self.input_services = input_services
        self.output_services = output_services
        self.internal_services = internal_services
        self.nodes = {}

    def load_services_from_db(self, activity_id, territory_id, capacity, territory_mapping=None, db_filename=""):

        path = os.path.abspath(os.path.dirname(__file__))
        if db_filename == "":
            db_filename = os.path.join(path, 'data', 'activities.db')
        connection = sqlite3.connect(db_filename)

        query = "SELECT * FROM activities WHERE activity = '{}' AND territory = '{}'"
        query = query.format(activity_id, territory_id)

        result = pd.read_sql_query(query, con=connection)
        result.drop("index", axis=1, inplace=True)
        
        connection.close()

        services = result.to_dict("records")
        
        self.input_services = []
        self.output_services = []
        self.internal_services = []
        
        for service in services:
            
            if service["flow_type"] in ["consumption", "capital_formation"]:
                
                if territory_mapping is None:
                    territory_name = service['territory']
                    territory = self.system.territories[territory_name]
                else:
                    territory_name = territory_mapping['inputs'][service['territory']]
                    territory = self.system.territories[territory_mapping['inputs'][territory_name]]
                    
                self.input_services.append(
                    InputService(
                        label = service['product'],
                        territory = territory,
                        conversion_factor = service['coefficient'],
                        flow_type = service['flow_type']
                    )
                )
                    
            elif service["flow_type"] in ["capital_consumption"]:
                    
                self.internal_services.append(
                    InternalService(
                        label = service['product'],
                        conversion_factor = service['coefficient'],
                        flow_type = service['flow_type']
                    )
                )
                    
            elif service["flow_type"] in ["production"]:
                
                if territory_mapping is None:
                    territory_name = service['territory']
                    territory = self.system.territories[territory_name]
                else:
                    territory_name = territory_mapping['outputs'][service['territory']]
                    territory = self.system.territories[territory_mapping['outputs'][territory_name]]
                    
                self.output_services.append(
                    OutputService(
                        label = service['product'],
                        territory = territory,
                        conversion_factor = service['coefficient'],
                        flow_type = service['flow_type'],
                        capacity = capacity
                    )
                )
                    
            else:
                ValueError(service["flow_type"] + " is not a valid flow_type (should be one of production, consumption, capital_formation, capital_consumption)")

        

import sqlite3
import os
import pandas as pd
import numpy as np
import json
import logging
import uuid

from scipy.sparse import coo_matrix

def load_supply_use_data(path, year):
    
    logging.info('Loading supply use product data')

    # --------------------------------------
    # IMPORT
    # Read input/output monetary eurostat data
    io = pd.read_csv(path, sep = '\t')
    io['unit'], io['stk_flow'], io['induse'], io['prod_na'], io['geo'] = io['unit,stk_flow,induse,prod_na,geo\\time'].str.split(',', 4).str
    io = io.drop(['unit,stk_flow,induse,prod_na,geo\\time'], axis=1)
    
    # Keep only french monetary flows for 2015
    io = io[io.geo == 'FR']
    io = io[io.unit == 'MIO_EUR']
    io = io[['unit','stk_flow','induse','prod_na','geo', str(year) + ' ']]
    
    io.columns = ['unit','stk_flow','induse','prod_na','geo','value']
    
    # Format data
    io['value'] = pd.to_numeric(io['value'], errors = 'coerce')
    io['induse'] = io['induse'].str.replace(' ', '')
    io['prod_na'] = io['prod_na'].str.replace(' ', '')
    
    return io[['stk_flow', 'induse', 'prod_na', 'value']]


    
def load_fixed_capital_delta_data(path, year):
    
    logging.info('Loading fixed assets data')

    # --------------------------------------
    # IMPORT
    # Read input/output monetary eurostat data
    nfc = pd.read_csv(path, sep = '\t')
    nfc['unit'], nfc['nace_r2'], nfc['asset10'], nfc['geo'] = nfc['unit,nace_r2,asset10,geo\\time'].str.split(',', 4).str
    nfc = nfc.drop(['unit,nace_r2,asset10,geo\\time'], axis=1)
    nfc = pd.melt(nfc, id_vars = ['unit', 'nace_r2', 'asset10', 'geo'])
    
    # Format data
    nfc['value'] = pd.to_numeric(nfc['value'], errors = 'coerce')
    nfc['year'] = nfc['variable'].astype('int64')
    nfc['nace_r2'] = nfc['nace_r2'].str.replace(' ', '')
    nfc['asset10'] = nfc['asset10'].str.replace(' ', '')
    
    # Keep only french monetary flows for 2015
    nfc = nfc[nfc.geo == 'FR']
    nfc = nfc[nfc.unit == 'CRC_MEUR']
    nfc = nfc[nfc.year.isin([year, year-1])]
    nfc = nfc[nfc['asset10'].str.strip().str[-1] == 'N']
    nfc['asset10'] = nfc['asset10'].apply(lambda x: str(x)[:-1])

    # Compute the difference of net fixed capital (= gfcf + cfc + adjustments)
    nfc = nfc.pivot_table(index=['nace_r2', 'asset10'], columns = 'year', values = 'value')
    nfc.reset_index(inplace=True)
    nfc['delta'] = nfc[year] - nfc[year-1]
    
    return nfc[['nace_r2', 'asset10', 'delta']]


def load_gfcf_data(path, year):
    
    logging.info('Loading GFCF data')

    # --------------------------------------
    # IMPORT
    # Read input/output monetary eurostat data
    gfcf = pd.read_csv(path, sep = '\t')
    gfcf['unit'], gfcf['nace_r2'], gfcf['asset10'], gfcf['geo'] = gfcf['unit,nace_r2,asset10,geo\\time'].str.split(',', 4).str
    gfcf = gfcf.drop(['unit,nace_r2,asset10,geo\\time'], axis=1)
    gfcf = pd.melt(gfcf, id_vars = ['unit', 'nace_r2', 'asset10', 'geo'])
    
    # Format data
    gfcf['value'] = pd.to_numeric(gfcf['value'], errors = 'coerce')
    gfcf['year'] = gfcf['variable'].astype('int64')
    gfcf['nace_r2'] = gfcf['nace_r2'].str.replace(' ', '')
    gfcf['asset10'] = gfcf['asset10'].str.replace(' ', '')
    gfcf['asset10'] = gfcf['asset10'].str.replace('G', '')
    
    # Keep only french monetary flows for 2015
    gfcf = gfcf[gfcf.geo == 'FR']
    gfcf = gfcf[gfcf.unit == 'CP_MEUR']
    gfcf = gfcf[gfcf.year == year]
    
    # Remvoe zero values
    gfcf = gfcf[gfcf.value > 0.0]
    gfcf['gfcf'] = gfcf['value']
    
    return gfcf[['nace_r2', 'asset10', 'gfcf']]

def load_employment_data(path,year):
    logging.info('Loading employment data')
    
    metadata = pd.read_excel(os.path.join(os.path.dirname(path),"metadata.xlsx"),sheet_name="nace_r2")

    jobs = pd.read_csv(path, sep = '\t')
    jobs['unit'], jobs['age'], jobs["sex"], jobs['nace_r2'], jobs['geo'] = jobs['unit,age,sex,nace_r2,geo\\time'].str.split(',', 5).str
    jobs = jobs.drop(['unit,age,sex,nace_r2,geo\\time'], axis=1)
    jobs = pd.melt(jobs, id_vars = ['nace_r2', "age", 'sex', "unit",'geo'])
    
    # Format data
    jobs['value'] = pd.to_numeric(jobs['value'], errors = 'coerce')
    jobs['year'] = jobs['variable'].astype('int64')
    jobs['nace_r2'] = jobs['nace_r2'].str.replace(' ', '')
    
    
    jobs = jobs[jobs.geo == 'FR']
    jobs = jobs[jobs.unit == 'THS'] #thousands person
    jobs = jobs[jobs.age == 'Y_GE15'] #15 or older
    jobs = jobs[jobs.sex == 'T']


    jobs = jobs[jobs.year == year]
    

    # Only actual nace categories without groups
    jobs = jobs[jobs.nace_r2.isin(metadata.nace_r2[metadata.Use == True])]
    
    
    # thousands to unit
    jobs["value"] = jobs["value"] * 1000
    

    jobs = jobs[jobs['value'] > 0.0]
    
    return jobs[['nace_r2', 'value']]

def load_energy_data(path,year):

    logging.info('Loading energy data')
    
    metadata = pd.read_excel(os.path.join(os.path.dirname(path), 'metadata.xlsx'),sheet_name="prod_nrg")
    metadata_nace = pd.read_excel(os.path.join(os.path.dirname(path), 'metadata.xlsx'),sheet_name="nace_r2")

    energy = pd.read_csv(path, sep = '\t')
    energy['stk_flow'], energy['nace_r2'], energy["prod_nrg"], energy['unit'], energy['geo'] = energy['stk_flow,nace_r2,prod_nrg,unit,geo\\time'].str.split(',', 5).str
    energy = energy.drop(['stk_flow,nace_r2,prod_nrg,unit,geo\\time'], axis=1)
    energy = pd.melt(energy, id_vars = ['stk_flow', 'nace_r2', "prod_nrg", 'unit', 'geo'])
    
    # Format data
    energy['value'] = pd.to_numeric(energy['value'], errors = 'coerce')
    energy['year'] = energy['variable'].astype('int64')
    energy['nace_r2'] = energy['nace_r2'].str.replace(' ', '')
    
    
    energy = energy[energy.geo == 'FR']
    energy = energy[energy.unit == 'TJ']
    energy = energy[energy.year == year]
    
    # Only actual energy source category
    energy = energy[energy.prod_nrg.isin(metadata.prod_nrg[metadata.Use == True])]
    # Only actual nace categories without groups
    energy = energy[energy.nace_r2.isin(metadata_nace.nace_r2[metadata_nace.Use == True])]
    
    
    
    # Only Emission-relevant use
    energy = energy[energy.stk_flow == "ER_USE"]
    
    # TJ to kWH
    energy["value"] = energy["value"] * 277777.77778 
    

    energy = energy[energy['value'] > 0.0]
    
    return energy[['nace_r2', 'prod_nrg', 'value']]


def get_ademe_ef(path,sector="energy",amont=False):
    ademe = pd.read_excel(os.path.join(path, 'data', 'ademe', 'basecarbone-acteurspublics.xlsx'))
    ademe_extra = pd.read_excel(os.path.join(path, 'data', 'ademe', 'basecarbone-manual_entries.xlsx'))
    ademe = pd.concat([ademe,ademe_extra],ignore_index=True)
    ademe = ademe.dropna(how='all', axis=1) # remove columns that are all empty
    ademe = ademe.replace(np.nan, '', regex=True)
    
    # Load Metadata 
    sheets = pd.ExcelFile(os.path.join(path, 'data', 'ademe', sector, 'metadata.xlsx')).sheet_names  # see all sheet names
    to_ademe = pd.read_excel(os.path.join(path, 'data', 'ademe', sector, 'metadata.xlsx'),sheet_name="to_ademe")
    to_ademe = to_ademe.replace(np.nan, '', regex=True)
    if 'Nom frontière français' in to_ademe.columns:
        to_ademe["Nom frontière français"] = to_ademe["Nom frontière français"].astype(str)
    parameters = pd.read_excel(os.path.join(path, 'data', 'ademe', sector, 'metadata.xlsx'),sheet_name="parameters")
    if "conversion" in sheets:
        conversion = pd.read_excel(os.path.join(path, 'data', 'ademe', sector, 'metadata.xlsx'),sheet_name="conversion")
    
    # Merge Eurostat with ADEME
    if 'Nom attribut français' in to_ademe.columns: #like for energy
        sel = pd.merge(ademe,to_ademe,
                       left_on=['Nom base français', 'Nom attribut français', "Nom frontière français"],
                       right_on=['identifiant', 'Nom attribut français', "Nom frontière français"])
    else: #like for waste
        sel = pd.merge(ademe,to_ademe,
                       left_on=['Nom base français'],
                       right_on=['identifiant'])
    
    # Convert to the same unit using metadata conversion table
    if "conversion" in sheets:
        for index, row in conversion.iterrows():
            sel.loc[sel["Unité français"] == row["From"],"Total poste non décomposé"] /= row["Factor"]
            sel = sel.replace({row["From"]:row["To"]})
    
    # Filter based on fields from metadata parameters
    for field in parameters.Field:
        value = parameters.loc[parameters.Field == field,"Value"].values[0]
        sel = sel[sel[field] == value]
    
    # Filter only elemnts classified as "amont" : 
    if amont:
        sel = sel[sel["Type poste"] == "Amont"]
    else:
        sel = sel[(sel["Type Ligne"] == "Elément")]
        
    #Filter based on status
    sel2 = sel[sel["Statut de l'élément"].str.startswith("Valide")]
    cats = sel[to_ademe.columns[0]].unique()[~np.isin(sel[to_ademe.columns[0]].unique(),sel2[to_ademe.columns[0]].unique())] # Find missing categories that do not have valid entries
    sel = pd.concat([sel2,sel[(~sel["Statut de l'élément"].str.startswith("Valide"))  & (sel[to_ademe.columns[0]].isin(cats))]],ignore_index=True)
    
    #Groupby the category and get the max (conservative)
    sel = sel.groupby(to_ademe.columns[0]).max().reset_index()

    ef = sel[[to_ademe.columns[0],"label","identifiant","Total poste non décomposé"]]
    
    ef = ef.rename({"Total poste non décomposé":"ef"},axis=1)
    ef = ef.set_index(to_ademe.columns[0]) #set the eurostat category as index
    
    return ef
    
    
def compute_emissions_from_energy(path, branch_energy):
    """Calculate emissions per activity based on energy use dataset and ADEME emission factors.
       This is alternative to using the emissions dataset with load_emissions_data()
    
     Args:
         path (str): path to the module
         branch_energy (pd.DataFrame): melted DataFrame with energy use [kWh] per NACE_2 activity and PROD_nrg energy source
    
     Returns:
         pd.Series: Emissions [kgCO2e] per NACE_2 activity
    
     """
    logging.info('Computing emissions from energy use')
    
    emission_factors =  pd.read_excel(os.path.join(path, 'data', 'eurostat', 'energy', 'emission_factors.xlsx'),index_col = [0])
    # Removing unnecessary column:  recuperation and natural
    emission_factors = emission_factors.drop(emission_factors.index[emission_factors.index.str.startswith("R")],axis=0)
    
    energy = branch_energy.pivot(index='nace_r2', columns='prod_nrg', values='value')
    # # Removing unnecessary column:  recuperation and natural
    energy = energy.drop(energy.columns[energy.columns.str.startswith("R")],axis=1)
    energy = energy.drop(energy.columns[energy.columns.str.startswith("N")],axis=1)
    
    total_energy= energy["P00"]
    energy = energy.drop("P00",axis=1) #
    energy["other"] = total_energy - energy.sum(axis=1)
    emission_factors.loc["other","emission_factor"] = 0.3120 # like gasoline for unknown values 
    
    energy_emissions = pd.DataFrame(index=energy.index,columns=energy.columns)
    for ensource in emission_factors.index:
        if ensource in energy.columns:
            energy_emissions.loc[:,ensource] = energy.loc[:,ensource] * emission_factors.loc[ensource,"emission_factor"]
        else:
            print("{} not in columns".format(ensource))
            
    # Prepare a DataFrame equivalent to the one from load_emissions_data()
    emissions = pd.DataFrame(index=energy_emissions.index)
    emissions["value"] = energy_emissions.sum(axis=1)
    emissions["airpol"] = "CO2E"
    emissions = emissions.reset_index()

    return emissions


def compute_emissions_from_waste(path, waste):
    """Calculate emissions per activity based on energy use dataset and ADEME emission factors.
       This is alternative to using the emissions dataset with load_emissions_data()
    
     Args:
         path (str): path to the module
         branch_waste (pd.DataFrame): melted DataFrame with waste weight [t] per NACE_2 activity and waste type
    
     Returns:
         pd.Series: Emissions [kgCO2e] per waste type
    
     """
    logging.info('Computing emissions from waste')
    
    # Load data
    #share = pd.read_excel(os.path.join(path,"data","ademe","waste","share.xlsx")) # share of waste treatment for each waste category
    md_map = pd.read_excel(os.path.join(path,"data","ademe","waste","conversion.xlsx")) # share of waste categories in domestic wastes
    md_map = md_map.dropna()
    md_map = md_map.rename({"value":"share"},axis=1)
    
    md_treat = pd.read_excel(os.path.join(path,"data","ademe","waste","metadata.xlsx"),sheet_name="treatment")
    # md_map = pd.read_excel(os.path.join(path,"data","ademe","waste","metadata.xlsx"),sheet_name=1)
    ademe = pd.read_excel(os.path.join(path,"data","ademe","basecarbone-acteurspublics.xlsx"))
    
    
    # Parse and clean waste from Eurostat
    waste = waste[waste.nace_r2 != "TOTAL_HH"] #remove total categories from Nace
    waste = waste.groupby("waste").sum().transpose() #get rid of Nace, and keep only total T per waste categories
    
    
    # Parse and clean ADEME db
    ademe = pd.merge(md_treat,ademe, left_on = "ademe", right_on ="Nom attribut français")
    ademe = pd.merge(md_map,ademe, left_on = "id_ademe", right_on ="Nom base français")
    ademe = ademe[ademe["Code de la catégorie"].str.startswith("Traitement des déchets")]
    ademe = ademe[ademe["Localisation géographique"].str.startswith("France")]
    ademe = ademe[ademe["Type Ligne"] == "Elément"]
    ademe = ademe[ademe["Unité français"] == "kgCO2e/tonne"]
    ademe = ademe[['id_eurostat_base', 'id_eurostat_final', 'id_ademe', 'treatment',"share",'Total poste non décomposé']]
    
    ademe = ademe.rename({'Total poste non décomposé':"ef"},axis=1)
    
    
    emissions = ademe.groupby(["id_ademe","treatment"],as_index=False).first()
    emissions = emissions[["id_ademe", "treatment","id_eurostat_base","ef"]]
    # # Start calculating the domestic wastes
    # domestic = domestic_share.copy()
    # domestic["value"] = domestic_share["value"] * (waste.W101.values[0] + waste.W102.values[0] + waste.W103.values[0])
    # domestic = pd.merge(md_map,domestic, left_on="id_ademe",right_on="id_ademe")
    # domestic["source"] = "domestic"
    

    # # Select only waste that have a mapping into the Ademe database
    # waste2 = pd.merge(waste.transpose().reset_index(),md_map.dropna(), on="waste")
    # waste2["source"] = "other"
    # # Create a detailed df cobining domestic and other waste
    # det_waste = pd.concat([waste2,domestic],axis=0,ignore_index=True,sort=True)
    # det_waste = det_waste.rename({"value":"quantity"},axis=1)
    

    # treatment = det_waste
    # # Calculate the emissions
    # treatment["emissions"] = treatment["quantity"]*treatment["share"]*treatment["CO2f"]
    # treatment["ef"] = treatment["share"]*treatment["CO2f"]
    
    # # emissions in kg CO2e
    # emissions = treatment[["waste","label","identifiant","treatment","source","emissions", "ef"]]

    return emissions

def compute_energy_intensity(io_branch_product, branch_energy, product_list, branch_list, economy_energy_branch_table):
    logging.info('Computing energy intensity ---- testing!')
    

    totals = branch_energy[branch_energy.prod_nrg == "P00"].groupby("nace_r2").sum()
    partials = branch_energy[branch_energy.prod_nrg != "P00"].groupby("nace_r2").sum()
    rest = totals-partials
    rest["prod_nrg"] = "P17" #transport diesel assigned to all non-classified energy
    rest = rest.reset_index()
    branch_energy = branch_energy[branch_energy.prod_nrg != "P00"] #remove P00 (total)
    branch_energy = pd.concat([branch_energy,rest],axis=0,ignore_index=True,sort=True)

    
    br_prod = io_branch_product[(io_branch_product['induse'].isin(branch_list['branch_id']) & (io_branch_product['prod_na'].isin(product_list['product_id'])))].copy()
    
    # Total production of the branch
    p_branch = br_prod.groupby(['induse'], as_index=False)['value'].sum()
    p_branch.columns = ['induse', 'p_j']
    
    # Total production of products
    p_prod = br_prod.groupby(['prod_na'], as_index=False)['value'].sum()
    p_prod.columns = ['prod_na', 'p_i']
    
    # Branch > product repartition coefficients and matrix
    a = br_prod.groupby(['induse', 'prod_na'], as_index=False)['value'].sum()
    a = pd.merge(a, p_branch, on = 'induse')
    a['a_ij'] = a['value']/a['p_j']
    
    a = pd.merge(a, branch_list[['branch_id', 'branch_index']], left_on = 'induse', right_on = 'branch_id')
    a = pd.merge(a, product_list[['product_id', 'product_index']], left_on = 'prod_na', right_on = 'product_id')
    
    N = product_list['product_index'].max()+1
    a_mat = coo_matrix((a['a_ij'], (a['product_index'], a['branch_index'])), shape=(N, N))
    
    # Energy
    energy_intensity = pd.DataFrame()
    energy_intensity["product_id"] = product_list['product_id']
    
    for ensource in branch_energy.prod_nrg.unique():

        sel_energy = branch_energy[branch_energy.prod_nrg == ensource]
        energy = pd.merge(sel_energy, economy_energy_branch_table, left_on = 'nace_r2', right_on = 'energy_branch_id', how = 'left')
        energy = energy.groupby(['economy_branch_id'], as_index=False)['value'].sum()
        energy.columns = ['induse', 'value']
        
        energy = energy[energy['induse'].isin(branch_list['branch_id'])]
        energy = pd.merge(energy, branch_list[['branch_id', 'branch_index']], left_on = 'induse', right_on = 'branch_id', how = 'left')
        energy_mat = coo_matrix((energy['value'], (energy['branch_index'], np.zeros(energy.shape[0]).astype(int))), shape = (N, 1))
        
        energy_mix = product_list[['product_id', 'product_index']]
        energy_mix[ensource] = np.matmul(a_mat.todense(), energy_mat.todense()[:, 0])

        energy_mix = pd.merge(energy_mix, p_prod, left_on = 'product_id', right_on = 'prod_na')
        energy_mix[ensource+'_contrib'] = energy_mix[ensource]/energy_mix['p_i']/1e6
        
        energy_mix = energy_mix[['product_id', ensource+'_contrib']]
        energy_intensity = pd.merge(energy_intensity,energy_mix,on="product_id")
        energy_intensity = energy_intensity.rename({ensource+'_contrib': ensource},axis=1)
        
    energy_intensity = energy_intensity.set_index("product_id")
    return energy_intensity

def compute_waste_intensity(io_branch_product, branch_waste, product_list, branch_list, economy_waste_branch_table):
    logging.info('Computing waste intensity ---- testing!')
    ######WIP##############
    br_prod = io_branch_product[(io_branch_product['induse'].isin(branch_list['branch_id']) & (io_branch_product['prod_na'].isin(product_list['product_id'])))].copy()
    
    # Total production of the branch
    p_branch = br_prod.groupby(['induse'], as_index=False)['value'].sum()
    p_branch.columns = ['induse', 'p_j']
    
    # Total production of products
    p_prod = br_prod.groupby(['prod_na'], as_index=False)['value'].sum()
    p_prod.columns = ['prod_na', 'p_i']
    
    # Branch > product repartition coefficients and matrix
    a = br_prod.groupby(['induse', 'prod_na'], as_index=False)['value'].sum()
    a = pd.merge(a, p_branch, on = 'induse')
    a['a_ij'] = a['value']/a['p_j']
    
    a = pd.merge(a, branch_list[['branch_id', 'branch_index']], left_on = 'induse', right_on = 'branch_id')
    a = pd.merge(a, product_list[['product_id', 'product_index']], left_on = 'prod_na', right_on = 'product_id')
    
    N = product_list['product_index'].max()+1
    a_mat = coo_matrix((a['a_ij'], (a['product_index'], a['branch_index'])), shape=(N, N))
    
    
    # Map nace_r2 categories
    branch_prod = io_branch_product.groupby("induse").sum()
    
    waste_to_economy = economy_waste_branch_table.copy()
    
    waste_to_economy = pd.merge(waste_to_economy,branch_prod,left_on="economy_branch_id", right_on="induse")
    waste_to_economy = waste_to_economy.set_index("waste_branch_id")
    waste_to_economy["ratio"] = waste_to_economy["value"] / waste_to_economy.groupby("waste_branch_id").sum()["value"]
    waste_to_economy = waste_to_economy.reset_index()
    
    branch_waste = pd.merge(branch_waste,waste_to_economy[["waste_branch_id","economy_branch_id","ratio"]],left_on="nace_r2",right_on="waste_branch_id")
    branch_waste["value"] = branch_waste["value"] * branch_waste["ratio"]
    
    branch_waste = branch_waste.drop("nace_r2", axis=1)
    branch_waste = branch_waste.rename({"economy_branch_id":"nace_r2"},axis=1)
    
    # Waste
    waste_intensity = pd.DataFrame()
    waste_intensity["product_id"] = product_list['product_id']
    
    for wastetype in branch_waste.waste.unique():

        sel_waste = branch_waste[branch_waste.waste == wastetype]
        #waste = pd.merge(sel_waste, economy_waste_branch_table, left_on = 'nace_r2', right_on = 'waste_branch_id', how = 'left')
        waste = sel_waste.groupby(['nace_r2'], as_index=False)['value'].sum()
        waste.columns = ['induse', 'value']
        
        waste = waste[waste['induse'].isin(branch_list['branch_id'])]
        waste = pd.merge(waste, branch_list[['branch_id', 'branch_index']], left_on = 'induse', right_on = 'branch_id', how = 'left')
        waste_mat = coo_matrix((waste['value'], (waste['branch_index'], np.zeros(waste.shape[0]).astype(int))), shape = (N, 1))
        
        waste_mix = product_list[['product_id', 'product_index']]
        waste_mix[wastetype] = np.matmul(a_mat.todense(), waste_mat.todense()[:, 0])

        waste_mix = pd.merge(waste_mix, p_prod, left_on = 'product_id', right_on = 'prod_na')
        waste_mix[wastetype+'_contrib'] = waste_mix[wastetype]/waste_mix['p_i']/1e6
        
        waste_mix = waste_mix[['product_id', wastetype+'_contrib']]
        waste_intensity = pd.merge(waste_intensity,waste_mix,on="product_id")
        waste_intensity = waste_intensity.rename({wastetype+'_contrib': wastetype},axis=1)
        
    waste_intensity = waste_intensity.set_index("product_id")
    
    return waste_intensity


def load_waste_data(path,year):
    logging.info('Loading waste data')

    metadata_waste = pd.read_excel(os.path.join(path,"data","eurostat","waste","Generation of waste by waste category, hazardousness and NACE Rev. 2 activity [env_wasgen]", 'metadata.xlsx'),sheet_name="waste")
    metadata_nace = pd.read_excel(os.path.join(path,"data","eurostat","waste","Generation of waste by waste category, hazardousness and NACE Rev. 2 activity [env_wasgen]", 'metadata.xlsx'),sheet_name="nace_r2")

    waste = pd.read_csv(os.path.join(path, 'data', 'eurostat', 'waste', 'Generation of waste by waste category, hazardousness and NACE Rev. 2 activity [env_wasgen]', "env_wasgen.tsv.gz"), sep = '\t')
    waste['unit'], waste['hazard'], waste['nace_r2'], waste["waste"], waste['geo'] = waste['unit,hazard,nace_r2,waste,geo\\time'].str.split(',', 5).str
    waste = waste.drop(['unit,hazard,nace_r2,waste,geo\\time'], axis=1)
    waste = pd.melt(waste, id_vars = ['unit', 'hazard', "nace_r2", 'waste', 'geo'])
    
    # # Format data
    waste['value'] = pd.to_numeric(waste['value'], errors = 'coerce')
    waste['year'] = waste['variable'].astype('int64')
    waste['nace_r2'] = waste['nace_r2'].str.replace(' ', '')
    
    
    waste = waste[waste.geo == 'FR']
    waste = waste[waste.unit == 'T']
    available_years = waste.year.unique()
    if year not in available_years:
        sel_year = year
        year = available_years[(np.abs(available_years - year)).argmin()]
        logging.info("Waste data not available for {}, using data of {}".format(sel_year,year))
    waste = waste[waste.year == year]
    
    # Hazardous and not hazardous
    waste = waste[waste.hazard == "HAZ_NHAZ"]
    
    # Only actual waste categories without groups
    waste = waste[waste.waste.isin(metadata_waste.waste[metadata_waste.Use == True])]
    # Only actual nace categories without groups
    waste = waste[waste.nace_r2.isin(metadata_nace.nace_r2[metadata_nace.Use == True])]
    
    waste = waste[waste['value'] > 0.0]
    
    # Return waste data in t
    return waste[['nace_r2', 'waste', 'value']]

def load_gfcf_data(path, year):
    
    logging.info('Loading GFCF data')

    # --------------------------------------
    # IMPORT
    # Read input/output monetary eurostat data
    gfcf = pd.read_csv(path, sep = '\t')
    gfcf['unit'], gfcf['nace_r2'], gfcf['asset10'], gfcf['geo'] = gfcf['unit,nace_r2,asset10,geo\\time'].str.split(',', 4).str
    gfcf = gfcf.drop(['unit,nace_r2,asset10,geo\\time'], axis=1)
    gfcf = pd.melt(gfcf, id_vars = ['unit', 'nace_r2', 'asset10', 'geo'])
    
    # Format data
    gfcf['value'] = pd.to_numeric(gfcf['value'], errors = 'coerce')
    gfcf['year'] = gfcf['variable'].astype('int64')
    gfcf['nace_r2'] = gfcf['nace_r2'].str.replace(' ', '')
    gfcf['asset10'] = gfcf['asset10'].str.replace(' ', '')
    gfcf['asset10'] = gfcf['asset10'].str.replace('G', '')
    
    # Keep only french monetary flows for 2015
    gfcf = gfcf[gfcf.geo == 'FR']
    gfcf = gfcf[gfcf.unit == 'CP_MEUR']
    gfcf = gfcf[gfcf.year == year]
    
    # Remvoe zero values
    gfcf = gfcf[gfcf.value > 0.0]
    gfcf['gfcf'] = gfcf['value']
    
    return gfcf[['nace_r2', 'asset10', 'gfcf']]



def compute_product_to_asset_coefficients(io, product_asset):
    
    logging.info('Computing GFCF')

    # Select consumption of government, households, NPISH, gross capital formation and exports
    gfcf_io = io[(io['induse'].isin(['P51G'])) & (io['prod_na'].isin(product_asset['prod_na']))].copy()
    gfcf_io = gfcf_io.groupby(['stk_flow', 'induse', 'prod_na'], as_index=False)['value'].sum()
    gfcf_io.columns = ['stk_flow', 'induse', 'prod_na', 'value']
    
    # Remove zero values
    gfcf_io.fillna(0.0, inplace=True)
    gfcf_io = gfcf_io[gfcf_io['value'] > 0.0]
    
    # Attribute each product to an asset
    gfcf_io = pd.merge(gfcf_io, product_asset, on = 'prod_na')
    
    # Compute the total asset formation flow
    gfcf_dom = gfcf_io.groupby('asset_id', as_index = False)['value'].sum()
    gfcf_dom.columns = ['asset_id', 'p_j']
    
    # Compute the coefficients
    gfcf_io = pd.merge(gfcf_io, gfcf_dom, on = 'asset_id')
    gfcf_io['a_ij'] = gfcf_io['value']/gfcf_io['p_j']
    
    
    gfcf_io['flow_type'] = 'consumption'
    gfcf_io = gfcf_io[['stk_flow', 'flow_type', 'asset_id', 'prod_na', 'a_ij']]

    # Rename columns
    gfcf_io.columns = ['origin', 'flow_type', 'output_product', 'input_product', 'a_ij']
    
    return gfcf_io


def compute_capital_flows_coefficients(io_product_product, asset_flows, asset_io, prod_40_64, flow_type):
    
    # Keep only assets from the product asset table
    asset_list = asset_io.output_product.unique()
    asset_flows = asset_flows[np.isin(asset_flows.asset10, asset_list)]
    asset_flows.columns = ['prod_na_asset', 'asset_id', 'value']
        
    # Split the assets from 40 to 64 levels (using total product production as key)
    total_prod = io_product_product[io_product_product.prod_na == 'P1'].groupby('induse', as_index=False)['value'].sum()
    
    prod_40_64 = pd.merge(prod_40_64, total_prod, left_on = 'prod_na', right_on = 'induse')
        
    prod_40 = prod_40_64.groupby('prod_na_asset', as_index=False)['value'].sum()
    prod_40.columns = ['prod_na_asset', 'value_tot']
        
    prod_40_64 = pd.merge(prod_40_64, prod_40, on = 'prod_na_asset')
    prod_40_64['k'] = prod_40_64['value']/prod_40_64['value_tot']
        
    prod_40_64 = prod_40_64[['prod_na_asset', 'prod_na', 'k']]
        
    asset_flows = pd.merge(asset_flows, prod_40_64, on = 'prod_na_asset')
    asset_flows['value'] *= asset_flows['k']
    asset_flows = asset_flows[['asset_id', 'prod_na', 'value']]
    asset_flows.columns = ['asset_id', 'prod_na', 'asset_value']
        
    asset_flows = pd.merge(asset_flows, total_prod, left_on = 'prod_na', right_on = 'induse')
    asset_flows['a_ij'] = asset_flows['asset_value']/asset_flows['value']
    
    asset_flows['origin'] = 'DOM'
    asset_flows['flow_type'] = flow_type
    
    asset_flows = asset_flows[['origin', 'flow_type', 'prod_na', 'asset_id', 'a_ij']]
    asset_flows.columns = ['origin', 'flow_type', 'output_product', 'input_product', 'a_ij']
    
    return asset_flows
 


 
    
def load_emissions_data(path, year):
    
    logging.info('Loading emissions data')
    
    emissions = pd.read_csv(path, sep = '\t')
    emissions['airpol'], emissions['nace_r2'], emissions['unit'], emissions['geo'] = emissions['airpol,nace_r2,unit,geo\\time'].str.split(',', 3).str
    emissions = emissions.drop(['airpol,nace_r2,unit,geo\\time'], axis=1)
    emissions = pd.melt(emissions, id_vars = ['airpol', 'nace_r2', 'unit', 'geo'])
    
    # Format data
    emissions['value'] = pd.to_numeric(emissions['value'], errors = 'coerce')
    emissions['year'] = emissions['variable'].astype('int64')
    emissions['nace_r2'] = emissions['nace_r2'].str.replace(' ', '')
    
    # Keep only CO2e emissions for 2015 (except CO2_BIO)
    emissions = emissions[emissions.geo == 'FR']
    # emissions = emissions[emissions.value != ': ']
    emissions = emissions[emissions.unit == 'T']
    emissions = emissions[emissions.year == year]
    emissions = emissions[emissions.airpol.isin(['CO2', 'CH4_CO2E', 'N2O_CO2E', 'HFC_CO2E', 'PFC_CO2E', 'NF3_SF6_CO2E'])]
    
    # Group all gases except biogenic carbon dioxyde
    emissions['airpol'] = 'CO2E'
    
    # Tons to kgCO2e
    emissions['value'] = emissions['value']*1000
    
    emissions = emissions.groupby(['nace_r2', 'airpol'], as_index=False)['value'].sum()
    emissions = emissions[emissions['value'] > 0.0]
    
    return emissions[['nace_r2', 'airpol', 'value']]
    
    
    
    
def compute_technical_coefficients(io, product_list):
    
    logging.info('Computing technical coefficients')
    
    # ------------------------------------------
    # Total domestic output
    dom_output = io[(io['stk_flow'] == 'DOM') & (io['prod_na'] == 'P1')]
    dom_output = dom_output.groupby(['induse'], as_index=False)['value'].sum()
    dom_output.columns = ['induse', 'p_j']
    
    # ------------------------------------------
    # Intermediate consumption
    ci = io[(io['induse'].isin(product_list['product_id'])) & (io['prod_na'].isin(product_list['product_id']))].copy()
    ci = ci.groupby(['stk_flow', 'induse', 'prod_na'], as_index=False)['value'].sum()
    ci.columns = ['stk_flow', 'induse', 'prod_na', 'ci_ij']    
    ci = pd.merge(ci, dom_output, on = ['induse'])
    ci['a_ij'] = ci['ci_ij']/ci['p_j']
    
    # Filter out zero values
    ci.fillna(0.0)
    ci = ci[ci['a_ij'] > 0.0]
    
    ci['flow_type'] = 'consumption'
    
    # Rename columns
    ci = ci[['stk_flow', 'flow_type', 'induse', 'prod_na', 'a_ij']]
    ci.columns = ['origin', 'flow_type', 'output_product', 'input_product', 'a_ij']
    
    return ci



def compute_final_consumption(io, product_list):
    
    logging.info('Computing final consumption')

    # Select consumption of government, households, NPISH and exports
    cf = io[(io['induse'].isin(['P3_S13', 'P3_S14', 'P3_S15'])) & (io['prod_na'].isin(product_list['product_id']))].copy()
    cf = cf.groupby(['stk_flow', 'induse', 'prod_na'], as_index=False)['value'].sum()
    cf.columns = ['stk_flow', 'induse', 'prod_na', 'value']
    
    # Compute the consumption ratio per inhabitant (€/person)
    cf['value'] = cf['value']/66.42
    
    # Remove zero values
    cf.fillna(0.0)
    cf = cf[cf['value'] > 0.0]
    
    # Rename columns
    cf.columns = ['origin', 'output_product', 'input_product', 'value']
    
    return cf



def compute_production_carbon_intensity(io_branch_product, branch_emissions, product_list, branch_list, economy_emissions_branch_table):
    
    logging.info('Computing carbon intensities')
    
    br_prod = io_branch_product[(io_branch_product['induse'].isin(branch_list['branch_id']) & (io_branch_product['prod_na'].isin(product_list['product_id'])))].copy()
    
    # Total production of the branch
    p_branch = br_prod.groupby(['induse'], as_index=False)['value'].sum()
    p_branch.columns = ['induse', 'p_j']
    
    # Total production of products
    p_prod = br_prod.groupby(['prod_na'], as_index=False)['value'].sum()
    p_prod.columns = ['prod_na', 'p_i']
    
    # Branch > product repartition coefficients and matrix
    a = br_prod.groupby(['induse', 'prod_na'], as_index=False)['value'].sum()
    a = pd.merge(a, p_branch, on = 'induse')
    a['a_ij'] = a['value']/a['p_j']
    
    a = pd.merge(a, branch_list[['branch_id', 'branch_index']], left_on = 'induse', right_on = 'branch_id')
    a = pd.merge(a, product_list[['product_id', 'product_index']], left_on = 'prod_na', right_on = 'product_id')
    
    N = product_list['product_index'].max()+1
    a_mat = coo_matrix((a['a_ij'], (a['product_index'], a['branch_index'])), shape=(N, N))
    
    # Emissions
    co2 = pd.merge(branch_emissions, economy_emissions_branch_table, left_on = 'nace_r2', right_on = 'emissions_branch_id', how = 'left')
    co2 = co2.groupby(['economy_branch_id'], as_index=False)['value'].sum()
    co2.columns = ['induse', 'value']
    
    co2 = co2[co2['induse'].isin(branch_list['branch_id'])]
    co2 = pd.merge(co2, branch_list[['branch_id', 'branch_index']], left_on = 'induse', right_on = 'branch_id', how = 'left')
    co2_mat = coo_matrix((co2['value'], (co2['branch_index'], np.zeros(co2.shape[0]).astype(int))), shape = (N, 1))
    
    
    carbon_intensity = product_list[['product_id', 'product_index']]
    carbon_intensity['emissions'] = np.matmul(a_mat.todense(), co2_mat.todense()[:, 0])
    carbon_intensity['airpol'] = 'CO2E'
    
    carbon_intensity = pd.merge(carbon_intensity, p_prod, left_on = 'product_id', right_on = 'prod_na')
    carbon_intensity['carbon_intensity'] = carbon_intensity['emissions']/carbon_intensity['p_i']/1e6
    
    carbon_intensity = carbon_intensity[['product_id', 'airpol', 'carbon_intensity']]
    carbon_intensity.columns = ['output_product', 'airpol', 'carbon_intensity']
    
    return carbon_intensity

def compute_employment_coefficients(io_branch_product, product_list, branch_list, branch_jobs, economy_jobs_branch_table):
    
    logging.info('Computing euros per job')
    
    
    branch_jobs = pd.merge(branch_jobs,economy_jobs_branch_table,left_on="nace_r2",right_on="employment_branch_id").groupby("economy_branch_id").sum()
    
    br_prod = io_branch_product[(io_branch_product['induse'].isin(branch_list['branch_id']) & (io_branch_product['prod_na'].isin(product_list['product_id'])))].copy()
    
    # Total production of the branch
    p_branch = br_prod.groupby(['induse'], as_index=False)['value'].sum()
    p_branch.columns = ['induse', 'p_j']
    
    # Total production of products
    p_prod = br_prod.groupby(['prod_na'], as_index=False)['value'].sum()
    p_prod.columns = ['prod_na', 'p_i']
    
    # Branch > product repartition coefficients and matrix
    a = br_prod.groupby(['induse', 'prod_na'], as_index=False)['value'].sum()
    a = pd.merge(a, p_branch, on = 'induse')
    a['a_ij'] = a['value']/a['p_j']
    
    a = pd.merge(a, branch_list[['branch_id', 'branch_index']], left_on = 'induse', right_on = 'branch_id')
    a = pd.merge(a, product_list[['product_id', 'product_index']], left_on = 'prod_na', right_on = 'product_id')
    
    a = pd.merge(a,branch_jobs,left_on="branch_id",right_on="economy_branch_id")
    a = a.rename({"value_x":"euros","value_y":"jobs"},axis=1)
    a["a_ij_2"] = a["euros"]/a["jobs"]
    
    employment_coefficients = a[["prod_na","a_ij_2"]]
    employment_coefficients = employment_coefficients.groupby("prod_na").sum(axis=0)
    employment_coefficients = employment_coefficients.rename({"a_ij_2":"a_ij"},axis=1)
    
    return employment_coefficients


def compute_households_direct_emissions(emissions):
    
    hh_em = emissions[emissions['nace_r2'].isin(['HH_HEAT', 'HH_TRA', 'HH_OTH'])].copy()
    hh_em['value'] = hh_em['value']/66.42/1e6
    
    hh_em = hh_em.rename({"nace_r2":'output_product'},axis=1)
    #hh_em.columns = ['output_product', 'airpol', 'value']
    
    return hh_em


def prepare_production_activities(technical_coefficients, carbon_intensity):
    
    activities = []

    # ---------------------------------------
    # Production activities
    services = technical_coefficients['output_product'].unique().tolist()

    for service in services:

        tc = technical_coefficients[technical_coefficients['output_product'] == service].to_dict('records')
        em = carbon_intensity[carbon_intensity['output_product'] == service].to_dict('records')
        
        # List the inputs
        inputs = []

        for v in tc:
            
            s = {}
            
            s['service'] = v['input_product']
            s['flow_type'] = v['flow_type']
            s['conversion_factor'] = v['a_ij']
            
            if v['origin'] == 'DOM':
                s['territory'] = 'france'
            else:
                s['territory'] = 'row'
                
            inputs.append(s)
        
        # Add GHG emissions
        if len(em) > 0:
            for e in em:
                if e['carbon_intensity'] > 0.0:
                    s = {}
                    s['service'] = e['airpol']
                    s['conversion_factor'] = e['carbon_intensity']
                    s['territory'] = 'biosphere'
                    s['flow_type'] = 'consumption'
                    inputs.append(s)

        outputs = [{'service': service, 'territory': 'france', 'conversion_factor': 1.0, 'capacity': 1.0, 'flow_type': 'production'}]

        activity = {}
        activity['id'] = str(uuid.uuid1())
        activity['type'] = 'production'
        activity['service'] = service
        activity['territory'] = 'france'


        activity['data'] = {}
        activity['data']['territory'] = 'france'
        activity['data']['inputs'] = inputs
        activity['data']['outputs'] = outputs
        
        
        activity['data'] = json.dumps(activity['data'])

        activities.append(activity)
    
    return activities

def prepare_energy_activities(technical_coefficients, carbon_intensity):
    ##############WIP 
    activities = []

    # ---------------------------------------
    # Production activities
    services = technical_coefficients['output_product'].unique().tolist()

    for service in services:

        tc = technical_coefficients[technical_coefficients['output_product'] == service].to_dict('records')
        em = carbon_intensity[carbon_intensity['output_product'] == service].to_dict('records')
        
        # List the inputs
        inputs = []

        for v in tc:
            
            s = {}
            
            s['service'] = v['input_product']
            s['flow_type'] = v['flow_type']
            s['conversion_factor'] = v['a_ij']
            
            if v['origin'] == 'DOM':
                s['territory'] = 'france'
            else:
                s['territory'] = 'row'
                
            inputs.append(s)
        
        # Add GHG emissions
        if len(em) > 0:
            for e in em:
                if e['carbon_intensity'] > 0.0:
                    s = {}
                    s['service'] = e['airpol']
                    s['conversion_factor'] = e['carbon_intensity']
                    s['territory'] = 'biosphere'
                    s['flow_type'] = 'consumption'
                    inputs.append(s)

        outputs = [{'service': service, 'territory': 'france', 'conversion_factor': 1.0, 'capacity': 1.0, 'flow_type': 'production'}]

        activity = {}
        activity['id'] = str(uuid.uuid1())
        activity['type'] = 'production'
        activity['service'] = service
        activity['territory'] = 'france'


        activity['data'] = {}
        activity['data']['territory'] = 'france'
        activity['data']['inputs'] = inputs
        activity['data']['outputs'] = outputs
        
        
        activity['data'] = json.dumps(activity['data'])

        activities.append(activity)
    ##############WIP
    return activities

def prepare_row_production_activities(technical_coefficients, carbon_intensity):
    
    activities = []
    
    # Sum domestic and and imports coefficients (= "rest of the world" has no imports)
    row_tc = technical_coefficients.groupby(['input_product', 'output_product', 'flow_type'], as_index=False)['a_ij'].sum()

    # ---------------------------------------
    # Production activities
    services = row_tc['output_product'].unique().tolist()

    for service in services:

        tc = row_tc[row_tc['output_product'] == service].to_dict('records')
        em = carbon_intensity[carbon_intensity['output_product'] == service].to_dict('records')
        
        # List the inputs
        inputs = []

        for v in tc:
            s = {}
            s['service'] = v['input_product']
            s['flow_type'] = v['flow_type']
            s['conversion_factor'] = v['a_ij']
            s['territory'] = 'row'
            inputs.append(s)
        
        # Add GHG emissions
        if len(em) > 0:
            for e in em:
                if e['carbon_intensity'] > 0.0:
                    s = {}
                    s['service'] = e['airpol']
                     # Multiply the carbon intensity by X to get a total carbon footprint compatible with SDES data (around 11.5 tCO2e/an)
                     # We should use a different correction factor for each service,
                     # or better still use input-output and emissions data from other countries !
                    s['conversion_factor'] = e['carbon_intensity']*2.75
                    s['flow_type'] = 'consumption'
                    s['territory'] = 'biosphere'
                    inputs.append(s)

        outputs = [{'service': service, 'territory': 'row', 'conversion_factor': 1.0, 'capacity': 1.0, 'flow_type': 'production'}]

        activity = {}
        activity['id'] = str(uuid.uuid1())
        activity['type'] = 'production'
        activity['service'] = service
        activity['territory'] = 'row'
        
        activity['data'] = {}
        activity['data']['territory'] = 'row'
        activity['data']['inputs'] = inputs
        activity['data']['outputs'] = outputs
        activity['data'] = json.dumps(activity['data'])

        activities.append(activity)
    
    return activities

def prepare_consumption_activites(final_consumption, households_direct_emissions):
    
    activities = []
    final_uses = final_consumption['output_product'].unique()
    
    for final_use in final_uses:
        
        fc = final_consumption[final_consumption['output_product'] == final_use].to_dict('records')

        inputs = []

        for v in fc:
            
            s = {}
            
            s['service'] = v['input_product']
            s['flow_type'] = 'consumption'
            s['conversion_factor'] = v['value']
            
            if v['origin'] == 'DOM':
                s['territory'] = 'france'
            else:
                s['territory'] = 'row'
                
            inputs.append(s)
            
        if final_use == 'P3_S14':
            
            hh_services = households_direct_emissions['output_product'].unique()
            
            for hh_service in hh_services:
            
                hh_em = households_direct_emissions[households_direct_emissions['output_product'] == hh_service].to_dict(orient='records')
                hh_inputs = []
                
                for em in hh_em:
                    if em['value'] > 0.0:
                        s = {}
                        s['service'] = em['airpol']
                        s['conversion_factor'] = em['value']
                        s['flow_type'] = 'consumption'
                        s['territory'] = 'biosphere'
                        hh_inputs.append(s)
                    
                s = {}
                s['service'] = em['output_product']
                s['flow_type'] = 'consumption'
                s['conversion_factor'] = 1.0
                s['territory'] = 'france'
                s['capacity'] = 1.0
                hh_outputs = [s]
                    
                activity = {}
                activity['id'] = str(uuid.uuid1())
                activity['type'] = 'production'
                activity['service'] = em['output_product']
                activity['territory'] = 'france'
            
                activity['data'] = {}
                activity['data']['territory'] = 'france'
                activity['data']['inputs'] = hh_inputs
                activity['data']['outputs'] = hh_outputs
                activity['data'] = json.dumps(activity['data'])
                    
                activities.append(activity)
                    
                # Add the service consumption
                s = {}
                s['service'] = em['output_product']
                s['flow_type'] = 'consumption'
                s['conversion_factor'] = 1.0
                s['territory'] = 'france'
                inputs.append(s)
            

        outputs = [{'service': final_use, 'territory': 'france', 'conversion_factor': 1.0, 'capacity': 1.0, 'flow_type': 'production'}]

        activity = {}
        activity['id'] = str(uuid.uuid1())
        activity['type'] = 'consumption'
        activity['service'] = final_use
        activity['territory'] = 'france'

        activity['data'] = {}
        activity['data']['territory'] = 'france'
        activity['data']['inputs'] = inputs
        activity['data']['outputs'] = outputs
        activity['data'] = json.dumps(activity['data'])

        activities.append(activity)
        
    return activities
    
def convert_eurostat_data(year=2015,emissions_source="default"):
    
    logging.info('Deprecated convert_eurostat_data: you should use create_db instead')

    logger = logging.getLogger()
    logger.setLevel(logging.INFO)
    
    path = os.path.abspath(os.path.dirname(__file__))
    
    # ---------------------------------
    # Activities
    branch_list = pd.read_excel(os.path.join(path, 'data', 'eurostat', 'economy', 'metadata.xlsx'), sheet_name='branch')
    product_list = pd.read_excel(os.path.join(path, 'data', 'eurostat', 'economy', 'metadata.xlsx'), sheet_name='product')
    
    io_product_product = load_supply_use_data(os.path.join(path, 'data', 'eurostat', 'economy', 'Symmetric input-output table at basic prices (product by product) (naio_10_cp1700)', 'naio_10_cp1700.tsv.gz'), 2015)
    io_product_product = io_product_product[io_product_product['stk_flow'] != 'TOTAL']
    
    io_branch_product = load_supply_use_data(os.path.join(path, 'data', 'eurostat', 'economy', 'Supply table at basic prices incl. transformation into purchasers\' prices (naio_10_cp15)', 'naio_10_cp15.tsv.gz'), 2015)
    
    technical_coefficients = compute_technical_coefficients(io_product_product, product_list)
    
    #Add employment
    branch_jobs = load_employment_data(os.path.join(path, 'data', 'eurostat', 'employment', 'lfsa_egan22d', "lfsa_egan22d.tsv.gz"),year)
    economy_jobs_branch_table = pd.read_excel(os.path.join(path, 'data', 'eurostat', 'employment', 'employment_to_economy.xlsx'))
    employment_coefficients = compute_employment_coefficients(io_branch_product, product_list, branch_list, branch_jobs, economy_jobs_branch_table)
 
    # ------------------------------
    # Final Consumption
    final_consumption = compute_final_consumption(io_product_product, product_list)

    # Extract the final consumption of households
    households_final_consumption = final_consumption[final_consumption['output_product'] == 'P3_S14']

    # Extract the final consumption of the government and NPISH
    gov_npish_io = final_consumption[final_consumption['output_product'].isin(['P3_S15', 'P3_S13'])]

    # Make their final consumption an intermediate consumption of households
    gov_npish_prod = gov_npish_io.groupby('output_product', as_index=False)['value'].sum()
    gov_npish_prod['origin'] = 'DOM'
    gov_npish_prod['input_product'] = gov_npish_prod['output_product']
    gov_npish_prod['output_product'] = 'P3_S14'
    
    households_final_consumption = pd.concat([households_final_consumption, gov_npish_prod], sort=True)

    # Compute their IO coefficients and add them as activities in the technical coefficients dataframe
    gov_npish_io = pd.merge(gov_npish_io, gov_npish_prod[['input_product', 'value']], left_on='output_product', right_on='input_product')
    gov_npish_io['a_ij'] = gov_npish_io['value_x']/gov_npish_io['value_y']
    gov_npish_io['flow_type'] = 'consumption'
    gov_npish_tech_coef = gov_npish_io[['origin', 'flow_type', 'input_product_x', 'output_product', 'a_ij']]
    gov_npish_tech_coef.columns = ['origin', 'flow_type', 'input_product', 'output_product', 'a_ij']

    # ------------------------------
    # Capital
    # Compute the coefficients to get from products to assets
    product_asset = pd.read_excel(os.path.join(path, 'data', 'eurostat', 'capital', 'metadata_.xlsx'), sheet_name='metadata')
    product_asset.dropna(inplace=True)
    asset_io = compute_product_to_asset_coefficients(io_product_product, product_asset)
    
    # Load the fixed capital formation of branches
    asset_formation = load_gfcf_data(os.path.join(path, 'data', 'eurostat', 'capital', 'Cross-classification of gross fixed capital formation by industry and by asset (flows) (nama_10_nfa_fl)', 'nama_10_nfa_fl.tsv.gz'), 2015)
    prod_40_64 = pd.read_excel(os.path.join(path, 'data', 'eurostat', 'capital', 'metadata_.xlsx'), sheet_name='na40_na64')
    capital_formation_coefficients = compute_capital_flows_coefficients(io_product_product, asset_formation, asset_io, prod_40_64, 'fixed_capital_formation')
    
    # Load the fixed capital consumption of branches
    assets_delta = load_fixed_capital_delta_data(os.path.join(path, 'data', 'eurostat', 'capital', 'Cross-classification of fixed assets by industry and by asset (stocks) (nama_10_nfa_st)', 'nama_10_nfa_st.tsv.gz'), 2015)

    cfc = pd.merge(asset_formation, assets_delta, on = ['nace_r2', 'asset10'])
    cfc['value'] = cfc['gfcf'] - cfc['delta']
    cfc = cfc[['nace_r2', 'asset10', 'value']]
    
    capital_consumption_coefficients = compute_capital_flows_coefficients(io_product_product, cfc, asset_io, prod_40_64, 'fixed_capital_consumption')


    def get_coefficients(intensity,flow_type):
        coefficients = intensity.reset_index().melt(id_vars=["product_id"])
        coefficients = coefficients.rename({"product_id":"output_product","variable":"input_product","value":"a_ij"},axis=1)
        coefficients = coefficients[coefficients.a_ij > 0]
        coefficients["origin"] = "IMP"
        coefficients["flow_type"] = "consumption"#flow_type
        coefficients = coefficients[["origin","flow_type","input_product","output_product","a_ij"]]

        return coefficients
    
    # Coefficients for energy and waste

    branch_energy = load_energy_data(os.path.join(path, 'data', 'eurostat', 'energy', 'Energy supply and use by NACE Rev. 2 activity', "env_ac_pefasu.tsv.gz"),year)
    economy_energy_branch_table = pd.read_excel(os.path.join(path, 'data', 'eurostat', 'energy', 'energy_to_economy.xlsx'))
    energy_intensity = compute_energy_intensity(io_branch_product, branch_energy, product_list, branch_list, economy_energy_branch_table)
    energy_coefficients = get_coefficients(energy_intensity, "energy")
    
    branch_waste = load_waste_data(path,year)
    economy_waste_branch_table = pd.read_excel(os.path.join(path, 'data', 'eurostat', 'waste', 'waste_to_economy.xlsx'))
    waste_intensity = compute_waste_intensity(io_branch_product, branch_waste, product_list, branch_list, economy_waste_branch_table)
    waste_coefficients = get_coefficients(waste_intensity, "waste")
    
    if emissions_source == "custom":
        # -------------------------------
        # Concatenate activities, capital and gov/NPISH activities, energy and waste
        technical_coefficients = pd.concat([
            technical_coefficients,
            asset_io,
            capital_formation_coefficients,
            capital_consumption_coefficients,
            gov_npish_tech_coef,
            energy_coefficients,
            waste_coefficients
        ], sort = True)
        

    branch_emissions = load_emissions_data(os.path.join(path, 'data', 'eurostat', 'emissions', 'Air emissions accounts by NACE Rev. 2 activity (env_ac_ainah_r2)', 'env_ac_ainah_r2.tsv.gz'), year)
    
    economy_emissions_branch_table = pd.read_excel(os.path.join(path, 'data', 'eurostat', 'emissions', 'emissions_to_economy.xlsx'))
    carbon_intensity = compute_production_carbon_intensity(io_branch_product, branch_emissions, product_list, branch_list, economy_emissions_branch_table)
    
    def adjust_carbon_intensity(path,sector,carbon_intensity,intensity):
        emission_factors = get_ademe_ef(path,sector,amont=False) #all emissions
        carbon_intensity = carbon_intensity.set_index("output_product") 
        if sector == "energy":             # Remove the direct emissions of the energy part
            emission_factors_direct = emission_factors.copy()
            emission_factors_direct["ef"] = emission_factors_direct["ef"].subtract(get_ademe_ef(path,sector,amont=True)["ef"]) #all emissions - amont
            emission_factors_direct = emission_factors_direct.fillna(0)
            carbon_intensity.loc[:,"carbon_intensity"] -= intensity.multiply(emission_factors_direct["ef"], axis=1).sum(axis=1) #kWh/Euros * kgCO2/kWh = kgCO2/Euros
        else: #Remove all the emissions
            carbon_intensity.loc[:,"carbon_intensity"] -= intensity.multiply(emission_factors["ef"], axis=1).sum(axis=1) #t/Euros * kgCO2/t = kgCO2/Euros (in case of waste)
        carbon_intensity = carbon_intensity.reset_index()
        
        return carbon_intensity        
    
    # Add the sector carbon intensity
        # sector_carbon_intensity = pd.DataFrame(columns=carbon_intensity.columns)
        # sector_carbon_intensity["output_product"] = intensity.index
        # sector_carbon_intensity["airpol"] = "CO2E"
        # sector_carbon_intensity["flow_type"] = sector
        # sector_carbon_intensity["carbon_intensity"] = intensity.multiply(emission_factors["ef"], axis=1).sum(axis=1).values
    
    if emissions_source == "custom":
        # carbon_intensity = adjust_carbon_intensity(path,"energy",carbon_intensity,energy_intensity)
        # carbon_intensity = adjust_carbon_intensity(path,"waste",carbon_intensity,waste_intensity)
        
        # Energy
        emission_factors = get_ademe_ef(path,"energy",amont=False) #all emissions
        emission_factors["input_product"] = "CO2E"
        emission_factors["output_product"] = emission_factors.index.values
        emission_factors["origin"] = "biosphere"
        emission_factors["a_ij"] = emission_factors["ef"]
        emission_factors["flow_type"] = "consumption"
        energy_tech_coefs = emission_factors[["origin", "flow_type", "input_product", "output_product", "a_ij"]]
        
        
        # Waste
        emission_factors = get_ademe_ef(path,"waste",amont=False)
        waste_emissions = compute_emissions_from_waste(path, branch_waste)
        waste_emissions.dropna(inplace=True)
        waste_emissions["input_product"] = "CO2E"
        waste_emissions["origin"] = "biosphere"
        waste_emissions["flow_type"] = "consumption"
        waste_emissions = waste_emissions.rename({"ef":"a_ij"},axis=1)
        waste_emissions = waste_emissions.drop(["id_ademe","treatment","id_eurostat_base"],axis=1)
        
        share = pd.read_excel(os.path.join(path,"data","ademe","waste","share.xlsx")) # share of waste treatment for each waste category
        share = pd.melt(share,id_vars=['identifiant'])
        share = share.rename({"variable":"input_product", "identifiant":"output_product", "value":"a_ij"},axis=1)
        share.dropna(inplace=True)
        share["origin"] = "DOM"
        share["flow_type"] = "consumption"
        share["input_product"] = share.apply(lambda x: x.input_product + "_" + x.output_product, axis=1)
        
        eurostat_to_ademe = pd.read_excel(os.path.join(path,"data","ademe","waste","conversion.xlsx")) # share 
        eurostat_to_ademe.dropna(inplace=True)
        
        waste_coef_mapped = pd.merge(waste_coefficients, eurostat_to_ademe[["id_eurostat_base", "id_ademe", "value"]], left_on = "input_product", right_on="id_eurostat_base")
        waste_coef_mapped["a_ij"] *= waste_coef_mapped["value"]
        waste_coef_mapped = waste_coef_mapped.groupby(["origin", "flow_type", "id_ademe", "output_product"], as_index=False)["a_ij"].sum()
        waste_coef_mapped.rename({"id_ademe": "input_product"}, axis=1, inplace=True)
    
    
    households_direct_emissions = compute_households_direct_emissions(branch_emissions)
    
    if emissions_source == "custom":
        technical_coefficients2 = pd.concat([technical_coefficients.reset_index(drop=True),share,waste_coef_mapped],ignore_index=True,sort=True)
        carbon_intensity2 = pd.concat([carbon_intensity,
                                       waste_emissions.rename({"input_product":"airpol","a_ij":"carbon_intensity"},axis=1).drop(["origin","flow_type"],axis=1),
                                       energy_tech_coefs.rename({"input_product":"airpol","a_ij":"carbon_intensity"},axis=1).drop(["origin","flow_type"],axis=1)],ignore_index=True,sort=True)
    
        prod_activities = prepare_production_activities(technical_coefficients2, carbon_intensity2)
        row_prod_activities = prepare_row_production_activities(technical_coefficients2, carbon_intensity2)
    else:
        prod_activities = prepare_production_activities(technical_coefficients, carbon_intensity)
        row_prod_activities = prepare_row_production_activities(technical_coefficients, carbon_intensity)   
        
    cons_activities = prepare_consumption_activites(households_final_consumption, households_direct_emissions)
    activities = prod_activities + row_prod_activities + cons_activities
    
    # Write employment to csv
    employment_coefficients.to_csv(os.path.join(path, 'data', 'eurostat', 'employment.csv'))
    
    # Write the results to the db
    db_filename = os.path.join(path, 'data', 'eurostat', 'eurostat_activities.db')
    connection = sqlite3.connect(db_filename)

    connection.execute("""
    DROP TABLE IF EXISTS eurostat_activities
    """)

    connection.execute("""
    CREATE TABLE IF NOT EXISTS eurostat_activities (
        id TEXT,
        service TEXT,
        territory TEXT,
        type TEXT,
        data TEXT
    )
    """)

    for activity in activities:
        connection.execute("""
        INSERT INTO eurostat_activities (id, service, territory, type, data)
        VALUES (?, ?, ?, ?, ?)
        """, (activity['id'], activity['service'], activity['territory'], activity['type'], activity['data']))
        connection.commit()

    connection.close()


# year = 2015
# path = os.path.abspath(os.path.dirname(__file__))
# branch_list = pd.read_excel(os.path.join(path, 'data', 'eurostat', 'economy', 'metadata.xlsx'), sheet_name='branch')
# product_list = pd.read_excel(os.path.join(path, 'data', 'eurostat', 'economy', 'metadata.xlsx'), sheet_name='product')
    
# branch_energy = load_energy_data(os.path.join(path, 'data', 'eurostat', 'energy', 'Energy supply and use by NACE Rev. 2 activity', "env_ac_pefasu.tsv.gz"),year)
# io_product_product = load_supply_use_product_product_data(os.path.join(path, 'data', 'eurostat', 'economy', 'Symmetric input-output table at basic prices (product by product) (naio_10_cp1700)', 'naio_10_cp1700.tsv.gz'), year)
# io_branch_product = load_supply_use_branch_product_data(os.path.join(path, 'data', 'eurostat', 'economy', 'Supply table at basic prices incl. transformation into purchasers\' prices (naio_10_cp15)', 'naio_10_cp15.tsv.gz'), year)
# economy_energy_branch_table = pd.read_excel(os.path.join(path, 'data', 'eurostat', 'energy', 'energy_to_economy.xlsx'))
# energy_intensity = compute_energy_intensity(io_branch_product, branch_energy, product_list, branch_list, economy_energy_branch_table)


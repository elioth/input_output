# -*- coding: utf-8 -*-
"""
Created on Wed May 27 15:21:40 2020
input_output is free software developed by Elioth (https://elioth.com/) ; you can redistribute it and/or modify it under the terms of the GNU General Public License
@author: louise
"""
from scipy.sparse import coo_matrix
import pandas as pd
import numpy as np

# Functions enabling the computation of the carbon footprint of every activity after an input_output computation

#data = x.copy()

def carbon_setup(data):

    
    # Extract all non zero flows from the system
    flows = data[data['value'] > 0].copy()
    
    # Fix storage calculation problem
    flows = flows.loc[flows['to_label']!='building_stock-charge']
    flows = flows.loc[flows['from_label']!='building_stock-charge']
    
    # Filter out storage charge flows to avoid calculation problem
    flows = flows[~flows['from_label'].str.contains("-charge")]
    flows = flows[~flows['to_label'].str.contains("-charge")]
    
    # Index the flows
    nodes_labels = np.unique(np.hstack((flows['from_label'], flows['to_label'])))
    nodes_index = pd.DataFrame({'label': nodes_labels, 'index': np.arange(0, len(nodes_labels)) })
    
    flows = pd.merge(flows, nodes_index, left_on = 'from_label', right_on = 'label')
    flows = pd.merge(flows, nodes_index, left_on = 'to_label', right_on = 'label')
    
    
    
    # Compute the technical coefficients of each flow (a_ij = input_i / total_output_j)
    a = flows.copy()

    p = a.groupby('from_label', as_index=False)['value'].sum()
    p.columns = ['to_label', 'total_supply']
    
    a = pd.merge(a, p, on='to_label')
    a['a_ij'] = a['value']/a['total_supply']
    
    # Extract co2 emissions and compute the direct carbon intensity of each flow
    liste = ['market_CO2_biosphere', 'market_HFC_CO2E_biosphere', 'market_NF3_SF6_CO2E_biosphere', 'market_PFC_CO2E_biosphere', 'market_CH4_CO2E_biosphere', 'market_N2O_CO2E_biosphere']
    co2 = flows[flows['from_label'].isin(liste)]
    #co2 = flows[(flows['from_region'] == 'biosphere')&(flows['to_region'] == 'biosphere')&(flows['from_label'] != 'CO2_BIO_production')]
    co2 = pd.merge(co2, p, on = 'to_label')
    co2['e_j'] = co2['value']/co2['total_supply']
    
    # Compute the emission factor of each flow
    # Create the technical coefficients matrix
    N = nodes_index.shape[0]
    a_mat = coo_matrix((a['a_ij'], (a['index_y'], a['index_x'])), shape=(N, N))
    a_mat.todense().shape
    
    # Create the direct emissions vector
    co2_mat = coo_matrix((co2['e_j'], (co2['index_y'], np.zeros(co2.shape[0]).astype(int))), shape = (N, 1))
    
    results = nodes_index.copy()
    results['emission_factor'] = np.matmul(np.linalg.inv(np.eye(N) - a_mat.todense()), co2_mat.todense()[:, 0])
    return flows, results, N

def carbon_footprint(flows, results, N, activity):
    
    # Extract the consumption flows and compute their allocated emissions
    hh = flows[flows['to_label'] == activity]
    hh_mat = coo_matrix((hh['value'], (hh['index_x'], np.zeros(hh.shape[0]).astype(int))), shape = (N, 1))
    

    results['households'] = results['emission_factor'].values*hh_mat.toarray()[:, 0]
    carbon = results['households'].sum()
    
    test = results.loc[results['households']>0]
    # categ = pd.read_excel('D:/elioth/input_output/categories.xlsx', usecols='A, C')
    # categ['code'] = categ['code'].str.split('_').str[0]
    # test['code'] = test['label'].str.split('_',3).str[2]
    # test = pd.merge(test, categ, on ='code', how='left')
    # view = test.groupby(['categ'], as_index=False)['households'].sum()
    return carbon, test


# nums=[0,1,2,3,6462]+list(range(6456,6461))+list(range(6306,6312))
# err = flows.loc[(flows['index_x'].isin(nums))&(flows['index_y'].isin(nums))]
import plotly.express as px
import plotly.graph_objects as go
def categories(data, path):
    data['code'] = data['label'].str.replace('market_', '').str.replace('_france', '').str.replace( '_neighborhood', '').str.replace( '_rest_of_the_world', '').str.replace( 'final_', '')
    data.loc[data['code'].str.contains('housing_production'), 'code'] = 'housing_production'
    categ = pd.read_excel(path+'200616 - Postes d\'émissions.xlsx', usecols='A, C, D')
    data = pd.merge(data, categ, on ='code', how='left')
    # data.loc[data['categ'].isna(), 'categ'] = 'batiment'
    # data.loc[data['sub_categ'].isna(), 'sub_categ'] = 'ACV + energie'
    
    data = data.groupby(['category_0', 'category_1'], as_index=False)['footprint'].sum()
    fig = px.bar(data, x='category_0', y='footprint', color ='category_1', text='category_1')
    fig.update_layout(uniformtext_minsize=8, uniformtext_mode='hide')
    #fig = go.Figure(data=[go.Bar(x=data['category_0'], y=data['footprint'], color =data['category_1'], text=data['category_1'])])

    # fig = go.Figure(data=[
    #     go.Bar(name='building', x=full_gp_bis.index, y=full_gp_bis.building, width=1),
    #     go.Bar(name='other', x=full_gp_bis.index, y=full_gp_bis.other, width=1)])
    fig.update_layout(title="Empreinte carbone moyenne", xaxis_title = "", yaxis_title="Empreinte carbone [kgCO2e/hh.an]", xaxis={'categoryorder':'array', 'categoryarray':['Transport','Bâtiment','Alimentation','Biens de consommation', 'Services']})
    fig.show(renderer='png')
    
    
    tmap = px.treemap(data, path=['category_0', 'category_1'], values = 'footprint')
    tmap.show(renderer='png')
    return fig, tmap

"""    
    
def full_carbon(data):
    # Extract all non zero flows from the system
    flows = data[data['value'] > 0].copy()
    
    # Filter out dummy capital formation flows to avoid double counting
    # flows = flows[~flows['from_label'].str.contains("equipment")]
    # flows = flows[~flows['to_label'].str.contains("equipment")]
    
    # Input / output table
    #io = flows.pivot_table(index='from', columns='to', values='value', fill_value=0)
    #flows.to_csv('io.csv')
    
    #products = pd.read_excel(os.path.join(package_path, 'input_output', 'data', 'metadata.xlsx'), sheet_name='product')
    
    # Index the flows
    nodes_labels = np.unique(np.hstack((flows['from_label'], flows['to_label'])))
    nodes_index = pd.DataFrame({'label': nodes_labels, 'index': np.arange(0, len(nodes_labels)) })
    
    flows = pd.merge(flows, nodes_index, left_on = 'from_label', right_on = 'label')
    flows = pd.merge(flows, nodes_index, left_on = 'to_label', right_on = 'label')
    
    #print(flows.head())
    

    # Compute the technical coefficients of each flow (a_ij = input_i / total_output_j)
    a = flows.copy()

    p = a.groupby('from_label', as_index=False)['value'].sum()
    p.columns = ['to_label', 'total_supply']
    
    a = pd.merge(a, p, on='to_label')
    a['a_ij'] = a['value']/a['total_supply']
    
    # Extract co2 emissions and compute the direct carbon intensity of each flow
    liste = ['market_CO2_biosphere', 'market_HFC_CO2E_biosphere', 'market_NF3_SF6_CO2E_biosphere', 'market_PFC_CO2E_biosphere', 'market_CH4_CO2E_biosphere', 'market_N2O_CO2E_biosphere']
    co2 = flows[flows['from_label'].isin(liste)]
    #co2 = flows[(flows['from_region'] == 'biosphere')&(flows['to_region'] == 'biosphere')&(flows['from_label'] != 'CO2_BIO_production')]
    co2 = pd.merge(co2, p, on = 'to_label')
    co2['e_j'] = co2['value']/co2['total_supply']
    
    # Compute the emission factor of each flow
    # Create the technical coefficients matrix
    N = nodes_index.shape[0]
    a_mat = coo_matrix((a['a_ij'], (a['index_y'], a['index_x'])), shape=(N, N))
    a_mat.todense().shape
    
    # Create the direct emissions vector
    co2_mat = coo_matrix((co2['e_j'], (co2['index_y'], np.zeros(co2.shape[0]).astype(int))), shape = (N, 1))
    
    results = nodes_index.copy()
    results['emission_factor'] = np.matmul(np.linalg.inv(np.eye(N) - a_mat.todense()), co2_mat.todense()[:, 0])
    
    # Extract the consumption flows and compute their allocated emissions
    hh = flows[flows['to_label'] == 'household_0']
    #hh = flows[flows['to_label'] == 'hh_france_demand']
    #hh = flows[flows['to_label'] == 'hh_activity_prod_france']
    hh_mat = coo_matrix((hh['value'], (hh['index_x'], np.zeros(hh.shape[0]).astype(int))), shape = (N, 1))
    

    results['households'] = results['emission_factor'].values*hh_mat.toarray()[:, 0]
    carbon = results['households'].sum()
    
    test = results.loc[results['households']>0]
    categ = pd.read_excel('D:/elioth/input_output/categories.xlsx', usecols='A, C')
    categ['code'] = categ['code'].str.split('_').str[0]
    test['code'] = test['label'].str.split('_',3).str[2]
    test = pd.merge(test, categ, on ='code', how='left')
    view = test.groupby(['categ'], as_index=False)['households'].sum()
    # results = results[results['label'].str.contains('node')].copy()
    # results.loc[results['label'].str.contains('row'), 'territory'] = 'row'
    # results.loc[results['label'].str.contains('france'), 'territory'] = 'france'
    # results.loc[results['label'].str.contains('neighborhood'), 'territory'] = 'neighborhood'
    # results['product_id'] = results['label'].str.replace('france_|row_|neighborhood_|_node', '')
    
    # results = pd.merge(results, products, on = 'product_id')
    
    # results[['product_id', 'label_fr', 'territory', 'households']].to_csv('footprint.csv', encoding='utf-8')


def carbon_bakery(data):
    # data is the result of refactoring_GP
    
    
    # Extract all non zero flows from the system
    flows = data[data['value'] > 0].copy()
    
    # Filter out dummy capital formation flows to avoid double counting
    # flows = flows[~flows['from_label'].str.contains("equipment")]
    # flows = flows[~flows['to_label'].str.contains("equipment")]
    
    # Input / output table
    #io = flows.pivot_table(index='from', columns='to', values='value', fill_value=0)
    #flows.to_csv('io.csv')
    
    #products = pd.read_excel(os.path.join(package_path, 'input_output', 'data', 'metadata.xlsx'), sheet_name='product')
    
    # Index the flows
    nodes_labels = np.unique(np.hstack((flows['from_label'], flows['to_label'])))
    nodes_index = pd.DataFrame({'label': nodes_labels, 'index': np.arange(0, len(nodes_labels)) })
    
    flows = pd.merge(flows, nodes_index, left_on = 'from_label', right_on = 'label')
    flows = pd.merge(flows, nodes_index, left_on = 'to_label', right_on = 'label')
    
    #print(flows.head())
    

    # Compute the technical coefficients of each flow (a_ij = input_i / total_output_j)
    a = flows.copy()

    p = a.groupby('from_label', as_index=False)['value'].sum()
    p.columns = ['to_label', 'total_supply']
    
    a = pd.merge(a, p, on='to_label')
    a['a_ij'] = a['value']/a['total_supply']
    
    # Extract co2 emissions and compute the direct carbon intensity of each flow
    co2 = flows[flows['from_label'] == 'co2_market']
    #co2 = flows[flows['from_region'] == 'biosphere']
    co2 = pd.merge(co2, p, on = 'to_label')
    co2['e_j'] = co2['value']/co2['total_supply']
    
    # Compute the emission factor of each flow
    # Create the technical coefficients matrix
    N = nodes_index.shape[0]
    a_mat = coo_matrix((a['a_ij'], (a['index_y'], a['index_x'])), shape=(N, N))
    a_mat.todense().shape
    
    # Create the direct emissions vector
    co2_mat = coo_matrix((co2['e_j'], (co2['index_y'], np.zeros(co2.shape[0]).astype(int))), shape = (N, 1))
    
    results = nodes_index.copy()
    results['emission_factor'] = np.matmul(np.linalg.inv(np.eye(N) - a_mat.todense()), co2_mat.todense()[:, 0])
    
    # Extract the consumption flows and compute their allocated emissions
    hh = flows[flows['to_label'] == 'consumer_demand']
    hh_mat = coo_matrix((hh['value'], (hh['index_x'], np.zeros(hh.shape[0]).astype(int))), shape = (N, 1))
    

    results['households'] = results['emission_factor'].values*hh_mat.toarray()[:, 0]
    
    # results = results[results['label'].str.contains('node')].copy()
    # results.loc[results['label'].str.contains('row'), 'territory'] = 'row'
    # results.loc[results['label'].str.contains('france'), 'territory'] = 'france'
    # results.loc[results['label'].str.contains('neighborhood'), 'territory'] = 'neighborhood'
    # results['product_id'] = results['label'].str.replace('france_|row_|neighborhood_|_node', '')
    
    # results = pd.merge(results, products, on = 'product_id')
    
    # results[['product_id', 'label_fr', 'territory', 'households']].to_csv('footprint.csv', encoding='utf-8')
    return results 
"""

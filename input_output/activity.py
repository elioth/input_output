#input_output is free software developed by Elioth (https://elioth.com/) ; you can redistribute it and/or modify it under the terms of the GNU General Public License

import uuid

class Activity():

    def __init__(self, name, region=None):
        """
            The Activity objects matches any activity of the real world that consumes and produce market products (an office, a bakery, an apartment building...).
            It stores all processes, capital stocks and internal markets that are necessary to the production of its output products.

            Args:
                name (str): name of the activity.
                region (Region): the region in which the activity is taking place.
        """
        self.name = name
        self.id = str(uuid.uuid1())
        self.region = region

        self.activities = {}
        self.processes = {}
        self.markets = {}

    def add_processes(self, processes):
        """
            Args:
                processes (list): the list of input / output processes that constitute the activity.
        """
        # Store each process in a dict, and reference the activity object in each process object
        for process in processes:
            process.system = self.region.system
            self.processes[process.name] = process


    def add_markets(self, markets):
        for market in markets:
            if self.region is not None:
                market.system = self.region.system
            else:
                market.system = self.system
            self.markets[market.name] = market

    def setup(self, oemof_system):
        """
            Add the oemof components (Bus, Flow, Transformer) to the oemof model, for each input_output component (Activity, Process, Market).

            Args:
                oemof_system: the oemof siumation object.
        """
        for k, market in self.markets.items():
            market.setup(oemof_system=oemof_system)

        for k, activity in self.activities.items():
            activity.setup(oemof_system=oemof_system)

        for k, process in self.processes.items():
            process.setup(oemof_system=oemof_system)


    def extract_results(self, results):
        """
            Extract the values of the flows of each process, activity.

            Args:
                results (pandas.DataFrame): dataframe of results produced by the solve method of the System object.
        """
        for k, process in self.processes.items():
            process.extract_results(results)

        for k, activity in self.activities.items():
            activity.extract_results(results)
            
    def localize_markets(self):
        """
            Create a dict {market_label: region}

            Returns:
                dict: locatization of markets
        """
        markets = {}
        for name in self.markets.keys():
            markets[name] = self.name
        return markets


class Region(Activity):

    def __init__(self, name):
        """
            The Region objects match geographical regions (a country, city, neighborhood...).
            They have the same properties and methods than activities, but have their own class to make the code clearer.
        """
        super().__init__(name)
        
    def add_activities(self, activities):
        for activity in activities:
            self.activities[activity.name] = activity
            activity.region = self.name
        
